# Hello World with Golang and Rabbit MQ

## How to started
1. Run Rabbit MQ server in docker
`docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management`
2. open terminal execute `go run main\retrieve.go`
3. open in another terminal, execute `go run main\send.go`